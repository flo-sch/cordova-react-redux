require('./check-versions')()

process.env.NODE_ENV = 'production'

var path          = require('path')
var ora           = require('ora')
var rm            = require('rimraf')
var chalk         = require('chalk')
var webpack       = require('webpack')
var webpackConfig = require('./webpack.prod.conf')
var config        = require('../config')

var spinner = ora('Building production version...');
spinner.start();

rm(path.join(config.build.assetsRoot, config.build.assetsSubDirectory), error => {
    if (error) {
        throw error;
    }

    webpack(webpackConfig, function (error, stats) {
        spinner.stop();

        if (error) {
            throw error;
        }

        process.stdout.write(stats.toString({
            colors: true,
            modules: false,
            children: false,
            chunks: false,
            chunkModules: false
        }) + '\n\n');

        console.log(chalk.cyan('  Build completed!\n'));
        console.log(chalk.yellow(
            '  Tip: built files are meant to be served over an HTTP server.\n' +
            '  Opening index.html over file:// won\'t work.\n'
        ));
    });
})
