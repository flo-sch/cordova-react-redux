import * as PlanningActionTypes from '@/actions/Planning';

const initialState = {
    days: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case PlanningActionTypes.PLANNING_LOADED:
            return Object.assign({}, state, {
                days: action.days
            });
        case PlanningActionTypes.PLANNING_LOADING_FAILED:
            return Object.assign({}, state, {
                days: []
            });
        default:
            return state;
    }
}
