import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { push } from 'react-router-redux';
import moment from 'moment';
import Hammer from 'hammerjs';

import { loadPlanning } from '@/actions/Planning';
import BackButton from '@/components/Partials/BackButton';
import Planning from '@/components/App/Planning/Calendar/Planning';

class PlanningComponent extends React.Component {

    constructor(props) {
        super(props);

        this.handleSwipe = this.handleSwipe.bind(this);
    }

    componentDidMount() {
        const { week } = this.props.match.params;

        /**
         * Asynchronous data loading
         *
         * This could be better, once asynchronous resolvers will be available with react-router@4.
         *
         * @__UPDATE_LATER__
         */
        this.props.loadPlanning(week);

        // Enable Swipe gesture thanks to HammerJS
        this.manager = new Hammer.Manager(ReactDOM.findDOMNode(this));
        const Swipe = new Hammer.Swipe({
            direction: Hammer.DIRECTION_HORIZONTAL,
            threshold: 10
        });
        this.manager.add(Swipe);

        // Listen to Swipe events
        this.manager.on('swipe', this.handleSwipe);
    }

    componentWillUnmount() {
        // Clear swipe event listener
        this.manager.off('swipe', this.handleSwipe);
    }

    /**
     * Week has been changed, force reload...
     *
     * This could be better, once asynchronous resolvers will be available with react-router@4.
     *
     * @__UPDATE_LATER__
     */
    componentWillReceiveProps(nextProps) {
        const routeChanged = nextProps.route !== this.props.route;

        if (routeChanged) {
            this.props.loadPlanning(nextProps.match.params.week);
        }
    }

    handleSwipe(event) {
        const week = parseInt(this.props.match.params.week);

        switch (event.direction) {
            case Hammer.DIRECTION_LEFT:
                this.props.loadWeek(week + 1);
                break;
            case Hammer.DIRECTION_RIGHT:
                this.props.loadWeek(week - 1);
                break;
        }
    }

    render() {
        const week = parseInt(this.props.match.params.week);
        const weekStart = new moment().startOf('year').add(week, 'weeks').startOf('week');
        const weekEnd = new moment().startOf('year').add(week, 'weeks').endOf('week');

        return (
            <section id='planning'>
                <header className='header gradient'>
                    <BackButton navigate={ this.props.navigate } />
                    <h2 className='header-title'>Mon planning</h2>
                </header>
                <div className='calendar-controls'>
                    <Link to={{ pathname: `/planning/${(week - 1)}` }} className='calendar-control-left'>
                        <span className='prh-icon prh-icon-angle rotate-90'></span>
                    </Link>
                    <h3 className='calendar-caption'>Semaine { week }</h3>
                    <p className='calendar-week-description'>du { weekStart.format('L') } au { weekEnd.format('L') }</p>
                    <Link to={{ pathname: `/planning/${(week + 1)}` }} className='calendar-control-right'>
                        <span className='prh-icon prh-icon-angle rotate-270'></span>
                    </Link>
                </div>
                <div className='planning-wrapper'>
                    <Planning days={ this.props.days } />
                </div>
            </section>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        route: state.routing.location.pathname,
        days: state.PlanningReducer.days
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigate: () => {
            dispatch(push('/'));
        },
        loadWeek: (week) => {
            dispatch(push(`/planning/${week}`));
        },
        loadPlanning: (week) => {
            dispatch(loadPlanning(week));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PlanningComponent);
