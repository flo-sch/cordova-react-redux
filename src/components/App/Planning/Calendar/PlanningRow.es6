import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

export default class PlanningRow extends React.Component {

    render() {
        return (
            <tr>
                <td className='planning-day-date'>{ this.props.date.format('ddd DD/MM') }</td>
                <td className='planning-day-places'>{ this.props.events.map((event, i) => {
                    return (
                        <div key={ i } className={ 'planning-day-place ' + event.type }>{ event.place }</div>
                    );
                }) }</td>
                <td className='planning-day-times'>{ this.props.events.map((event, i) => {
                    return (
                        <div key={ i } className='planning-day-time'>{ event.time }</div>
                    );
                }) }</td>
            </tr>
        )
    }

}

PlanningRow.propTypes = {
    date: PropTypes.instanceOf(moment).isRequired,
    events: PropTypes.arrayOf(PropTypes.shape({
        type: PropTypes.string.isRequired,
        place: PropTypes.string.isRequired,
        time: PropTypes.string.isRequired
    })).isRequired
};
