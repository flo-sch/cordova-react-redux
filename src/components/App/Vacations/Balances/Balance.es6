import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardTitle, CardBlock, Collapse } from 'reactstrap';

export default class Balance extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isCollapsiblePanelOpen: false
        };

        this.collapse = this.collapse.bind(this);
    }

    collapse() {
        this.setState({
            isCollapsiblePanelOpen: !this.state.isCollapsiblePanelOpen
        });
    }

    render() {
        return (
            <Card className='balance'>
                <CardBlock>
                    <CardTitle onClick={ this.collapse }>
                        <span className={ 'prh-icon prh-icon-angle' + (this.state.isCollapsiblePanelOpen ? '' : ' rotate--90') }></span>
                        { this.props.name }
                    </CardTitle>
                    <Collapse isOpen={ this.state.isCollapsiblePanelOpen }>
                        <table className='balances-table'>
                            <thead>
                                <tr>
                                    <th>Descriptif</th>
                                    <th className='text-right'>Solde</th>
                                    <th className='text-right'>A terme</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.props.history.map((history, i) => {
                                    return (
                                        <tr key={ i }>
                                            <td>{ history.name }</td>
                                            <td className='text-right'>{ history.balance.toFixed(2) } j</td>
                                            <td className='text-right'>{ history.futureBalance.toFixed(2) } j</td>
                                        </tr>
                                    )
                                }) }
                            </tbody>
                        </table>
                    </Collapse>
                </CardBlock>
            </Card>
        );
    }
}

Balance.propTypes = {
    name: PropTypes.string.isRequired,
    history: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        balance: PropTypes.number.isRequired,
        futureBalance: PropTypes.number.isRequired
    })).isRequired
};
