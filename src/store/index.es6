import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import History from '@/middlewares/History';
import RootReducer from '@/reducers';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    RootReducer,
    /* preloadedState, */
    composeEnhancers(
        applyMiddleware(
            thunkMiddleware,
            createLogger(),
            routerMiddleware(History),
        ),
    ),
);
