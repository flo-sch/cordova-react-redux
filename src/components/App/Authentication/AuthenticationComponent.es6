import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Collapse } from 'reactstrap';

import User from '@/models/User';
import { authenticate, switchAccount } from '@/actions/Authentication';

class AuthenticationComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: props.user ? props.user.username : '',
            password: '',
            companyCode: props.companyCode || '',
            collapsed: true
        };

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onCompanyCodeChange = this.onCompanyCodeChange.bind(this);

        this.onSwitchAccountButtonClicked = this.onSwitchAccountButtonClicked.bind(this);
        this.handleSubmit = this.onFormSubmitted.bind(this);
        this.onCollapse = this.onCollapse.bind(this);
    }

    onUsernameChange(event) {
        this.setState({
            username: event.target.value
        });
    }

    onPasswordChange(event) {
        this.setState({
            password: event.target.value
        });
    }

    onCompanyCodeChange(event) {
        this.setState({
            companyCode: event.target.value
        });
    }

    onFormSubmitted(event) {
        event.preventDefault();

        this.props.handleAuthentication(this.state.username, this.state.password, this.state.companyCode);
    }

    onSwitchAccountButtonClicked(event) {
        event.preventDefault();

        this.props.handleSwitchAccount();
    }

    onCollapse() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    get avatar() {
        let avatar = <img className='avatar' src={ require('@/static/images/avatar-placeholder.svg') } alt='Avatar' />;
        let userInformations = null;

        const { user } = this.props;

        if (user) {
            userInformations = (
                <figcaption className='avatar-caption'>
                    <h6>Bonjour</h6>
                    <h4>{ user.firstname }</h4>
                </figcaption>
            );

            if (user.avatar) {
                avatar = <img className='avatar' src={ user.avatar } alt='Avatar' />;
            }
        }

        return (
            <figure className='avatar-container'>
                { avatar }
                { userInformations }
            </figure>
        );
    }

    get fields() {
        const { user, companyCode } = this.props;
        const username = user ? user.username : '';

        // let usernameFieldGroup = null;
        let usernameFieldGroup = (<div className='form-group'>
                <input type='text' name='username' placeholder='identifiant' className='form-control' onChange={ this.onUsernameChange } defaultValue={ username } />
            </div>);

        let passwordFieldGroup = (<div className='form-group'>
            <input type='password' name='password' placeholder='mot de passe' className='form-control' onChange={ this.onPasswordChange } />
        </div>);

        let companyCodeFieldGroup = (<div className='form-group'>
            <input type='text' name='companyCode' placeholder='Code établissement' className='form-control' onChange={ this.onCompanyCodeChange } defaultValue={ companyCode } />
        </div>);

        let collapsibleGroup = null;

        if (user) {
            usernameFieldGroup = null;

            collapsibleGroup = (
                <div className='form-group'>
                    <Collapse isOpen={ !this.state.collapsed }>
                        { companyCodeFieldGroup}
                        <div className='form-group'>
                            <a className='btn btn-block btn-outline-secondary' onClick={ this.onSwitchAccountButtonClicked }>Se connecter avec un autre compte</a>
                        </div>
                    </Collapse>
                    <a onClick={ this.onCollapse }>
                        <span className={ 'prh-icon prh-icon-angle' + (!this.state.collapsed ? ' rotate-180' : '') }></span>
                    </a>
                </div>
            );
        } else {
            collapsibleGroup = companyCodeFieldGroup;
        }

        return (
            <fieldset>
                { usernameFieldGroup }
                { passwordFieldGroup }
                { collapsibleGroup }
            </fieldset>
        );
    }

    render() {
        const isSubmitAllowed = (
            !this.props.user && !!this.state.username && !!this.state.password && !!this.state.companyCode
            ||
            this.props.user && !!this.state.password && !!this.state.companyCode
        );

        return (
            <section id='login' className='full gradient'>
                <div className='container-fluid text-center'>
                    <h1 className='background-text'>LOGIN</h1>
                    <form className='transparent' onSubmit={ this.handleSubmit }>
                        <img src={ require('@/static/images/logo-baseline.svg') } height='50' alt='Logo' />
                        { this.avatar }
                        { this.fields }
                        <div className='form-group text-center'>
                            <button type='submit' className='btn btn-block btn-default' disabled={ !isSubmitAllowed }>Se connecter</button>
                            <a href='#'>mot de passe oublié ?</a>
                        </div>
                    </form>
                </div>
            </section>
        )
    }
}

AuthenticationComponent.propTypes = {
    user: PropTypes.instanceOf(User),
    companyCode: PropTypes.string
};

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.AuthenticationReducer.isAuthenticated,
        user: state.AuthenticationReducer.user,
        companyCode: state.AuthenticationReducer.companyCode,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        handleAuthentication: (username, password, companyCode) => {
            dispatch(authenticate(username, password, companyCode));
        },
        handleSwitchAccount: () => {
            dispatch(switchAccount());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(AuthenticationComponent);
