// import { createBrowserHistory } from 'history';
import { createHashHistory } from 'history';

// const History = createBrowserHistory();
const History = createHashHistory();

export default History;
