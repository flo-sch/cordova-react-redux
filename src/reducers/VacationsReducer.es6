import * as VacationsActionTypes from '@/actions/Vacations';

const initialState = {
    vacations: {
        pending: [],
        mine: []
    },
    vacation: null,
    currentBalance: null,
    simulatedBalance: null,
    isSimulatedResultAllowed: false,
    planning: null // Team planning
};

export default (state = initialState, action) => {
    switch (action.type) {
        case VacationsActionTypes.VACATIONS_LOADED:
            return Object.assign({}, state, {
                vacations: {
                    pending: action.vacations.pending,
                    mine: action.vacations.mine
                }
            });
        case VacationsActionTypes.VACATIONS_LOADING_FAILED:
            return Object.assign({}, state, {
                vacations: {
                    pending: [],
                    mine: []
                }
            });
        case VacationsActionTypes.TEAM_PLANNING_LOADED:
            return Object.assign({}, state, {
                planning: action.planning
            });
        case VacationsActionTypes.TEAM_PLANNING_LOADING_FAILED:
            return Object.assign({}, state, {
                planning: null
            });
        case VacationsActionTypes.VACATION_SIMULATED:
            return Object.assign({}, state, {
                vacation: null,
                currentBalance: action.currentBalance,
                simulatedBalance: action.simulatedBalance,
                isSimulatedResultAllowed: action.isSimulatedResultAllowed
            });
        case VacationsActionTypes.VACATION_CREATED:
            return Object.assign({}, state, {
                vacation: action.vacation,
                currentBalance: action.balance,
                simulatedBalance: null,
                isSimulatedResultAllowed: false
            });
        // At the moment, both simulation and creation failures can be handled in the same way...
        // --> It does not involve any state change for now.
        case VacationsActionTypes.VACATION_SIMULATION_FAILED:
        case VacationsActionTypes.VACATION_CREATION_FAILED:
        default:
            return state;
    }
}
