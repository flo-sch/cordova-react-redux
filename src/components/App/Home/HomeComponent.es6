import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';

import { loadVacations } from '@/actions/Vacations';

class HomeComponent extends React.Component {

    componentDidMount() {
        this.props.loadVacations();
    }

    render() {
        const { pendingVacations } = this.props;

        return (
            <section id='home'>
                <header className='header header-lg gradient'>
                    <div className='container-fluid'>
                        <h2 className='header-title'>Accueil</h2>
                        <div className='row background-logo' style={{ backgroundImage: `url(${require('@/static/images/logo.svg')})` }}>
                            <div className='col-6'>
                                <Link to='/time-off/new' className='wrapper-link'>
                                    <figure className='header-cta primary'>
                                        <span className='header-cta-icon prh-icon prh-icon-reminder'></span>
                                        <figcaption className='header-label'>Nouvelle<br />demande d'absence</figcaption>
                                        <button type='button' className='btn btn-sm btn-rounded btn-header btn-outline-primary'>
                                            <span className='prh-icon prh-icon-arrow'></span>
                                        </button>
                                    </figure>
                                </Link>
                            </div>
                            <div className='col-6'>
                                <Link to='/expenses/new' className='wrapper-link'>
                                    <figure className='header-cta secondary'>
                                        <span className='header-cta-icon prh-icon prh-icon-euro'></span>
                                        <figcaption className='header-label'>Nouvelle<br />dépense</figcaption>
                                        <button type='button' className='btn btn-sm btn-rounded btn-header btn-outline-secondary'>
                                            <span className='prh-icon prh-icon-arrow'></span>
                                        </button>
                                    </figure>
                                </Link>
                            </div>
                        </div>
                        <Link to={{ pathname: `/planning/${(new moment()).format('w')}` }} className='btn btn-lg btn-info main-btn'>Planning</Link>
                    </div>
                </header>
                <div className='cards-list'>
                    <Link to='/time-off/pending' className='wrapper-link block'>
                        <div className='card'>
                            <div className='card-block'>
                                <div className='media'>
                                    <div className='media-body'>
                                        <h4 className='card-title'>{ pendingVacations.length }</h4>
                                        <p className='card-text'>absences à valider</p>
                                    </div>
                                    <span className='d-flex align-self-center'>
                                        <span className='round primary'>
                                            <span className='prh-icon prh-icon-angle rotate-270'></span>
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </Link>
                    <div className='card card-muted'>
                        <div className='card-block'>
                            <div className='media'>
                                <div className='media-body'>
                                    <h4 className='card-title'>20</h4>
                                    <p className='card-text'>notes à frais</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='card card-muted'>
                        <div className='card-block'>
                            <div className='media'>
                                <div className='media-body'>
                                    <h4 className='card-title'>09</h4>
                                    <p className='card-text'>entretiens à valider</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='card card-muted'>
                        <div className='card-block'>
                            <div className='media'>
                                <div className='media-body'>
                                    <h4 className='card-title'>07</h4>
                                    <p className='card-text'>évaluations de formation</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        pendingVacations: state.VacationsReducer.vacations.pending
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadVacations: () => {
            dispatch(loadVacations());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeComponent);
