import { push } from 'react-router-redux';

// import DataProvider from '@/middlewares/Data/ApiClient';
import DataProvider from '@/middlewares/Data/StaticDataProvider';
import Storage from '@/middlewares/Storage/PersistentStorage';

/**
 * Action types
 */
export const AUTHENTICATION_SUCCESS = 'authentication.success';
export const AUTHENTICATION_ERROR = 'authentication.error';
export const DISCONNECTION = 'disconnection';

const onAuthenticationSuccess = (token, user) => {
    return {
        type: AUTHENTICATION_SUCCESS,
        token,
        user
    };
}

const onAuthenticationError = (error) => {
    return {
        type: AUTHENTICATION_ERROR,
        error
    };
}

/**
 * Action creators
 */
export const authenticate = (username, password, companyCode) => {
    return dispatch => {
        // Call WS
        DataProvider
            .authenticate(username, password, companyCode)
            .then(result => {
                const { token, user } = result;

                if (token && user) {
                    Storage.set('token', token);
                    Storage.set('user', JSON.stringify(user));
                    Storage.set('company_code', companyCode);

                    dispatch(onAuthenticationSuccess(token, user));
                    return dispatch(push('/'));
                } else {
                    return dispatch(onAuthenticationError(new Error('Unable to parse DataProvider response...')));
                }
            })
            .catch(error => {
                return dispatch(onAuthenticationError(error));
            })
        ;
    }
}

export const SWITCH_ACCOUNT_SUCCESS = 'account.switched';

const onSwitchAccountSuccess = () => {
    return {
        type: SWITCH_ACCOUNT_SUCCESS
    };
}

export const switchAccount = () => {
    return dispatch => {
        Storage.remove('token');
        Storage.remove('user');

        return dispatch(onSwitchAccountSuccess());
    }
}
