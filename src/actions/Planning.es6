// import DataProvider from '@/middlewares/Data/ApiClient';
import DataProvider from '@/middlewares/Data/StaticDataProvider';

/**
 * Action types
 */
export const PLANNING_LOADED            = 'planning.loaded';
export const PLANNING_LOADING_FAILED    = 'planning.load_failed';

const onPlanningLoaded = (days) => {
    return {
        type: PLANNING_LOADED,
        days
    };
}

const onPlanningLoadFailed = (error) => {
    return {
        type: PLANNING_LOADING_FAILED,
        error
    };
}

/**
 * Action creators
 */
export const loadPlanning = (week) => {
    return dispatch => {
        // Call WS
        DataProvider
            .loadPlanning(week)
            .then(result => {
                const { days } = result;

                return dispatch(onPlanningLoaded(days));
            })
            .catch(error => {
                return dispatch(onPlanningLoadFailed(error));
            })
        ;
    }
}
