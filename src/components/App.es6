import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import classnames from 'classnames';

import Loader from '@/components/Partials/Loader';

class App extends React.Component {

    render() {
        const { isAnyRequestLoading, isAuthenticated, children, uri } = this.props;

        let navbar = null;
        let loader = null;

        if (isAnyRequestLoading) {
            loader = <Loader />;
        }

        if (isAuthenticated) {
            navbar = (
                <nav id='menu-navbar' className='navbar fixed-bottom navbar-inverse bg-inverse'>
                    <ul className='navbar-nav'>
                        <li className={ 'nav-item' + (new RegExp(/^\/$/).test(uri) ? ' active' : '') }>
                            <Link to='/' className='nav-link'>
                                <span className='prh-icon prh-icon-home'></span>
                            </Link>
                        </li>
                        <li className={ 'nav-item' + (new RegExp(/^\/(time-off|planning)/).test(uri) ? ' active' : '') }>
                            <Link to='/time-off' className='nav-link'>
                                <span className='prh-icon prh-icon-reminder'></span>
                            </Link>
                        </li>
                        <li className={ 'nav-item' + (new RegExp(/^\/expenses/).test(uri) ? ' active' : '') }>
                            <Link to='/expenses' className='nav-link'>
                                <span className='prh-icon prh-icon-euro'></span>
                            </Link>
                        </li>
                        <li className={ 'nav-item' + (new RegExp(/^\/meetings/).test(uri) ? ' active' : '') }>
                            <Link to='/meetings' className='nav-link'>
                                <span className='prh-icon prh-icon-handshake'></span>
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <a className='nav-link'>
                                <span className='prh-icon prh-icon-menu'></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            );
        }

        return (
            <main id='app' className={ classnames({
                    'authenticated': isAuthenticated,
                    'navbar-hidden-landscape': new RegExp(/^\/time-off\/pending\/calendar/).test(uri)
                }) }>
                { children }
                { navbar }
                { loader }
            </main>
        );
    }

}

function mapStateToProps(state) {
    return {
        isAuthenticated: state.AuthenticationReducer.isAuthenticated,
        isAnyRequestLoading: state.HttpReducer.isLoading,
        uri: state.routing.location.pathname,
    };
}

export default withRouter(connect(
    mapStateToProps
)(App));
