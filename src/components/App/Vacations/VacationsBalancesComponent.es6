import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { loadBalances } from '@/actions/Balances';
import BackButton from '@/components/Partials/BackButton';
import Balance from '@/components/App/Vacations/Balances/Balance';

class VacationsBalancesComponent extends React.Component {

    componentDidMount() {
        this.props.loadBalances();
    }

    render() {
        return (
            <section id='vacations-balances'>
                <header className='header gradient'>
                    <BackButton />
                    <h2 className='header-title'>Mes soldes</h2>
                </header>
                { this.props.balances.map((balance, i) => {
                    return <Balance
                        key={ i }
                        name={ balance.name }
                        history={ balance.history }
                        />
                }) }
            </section>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        balances: state.BalancesReducer.balances
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadBalances: () => {
            dispatch(loadBalances());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VacationsBalancesComponent);
