import * as AuthenticationActionTypes from '@/actions/Authentication';
import Storage from '@/middlewares/Storage/PersistentStorage';
import User from '@/models/User';

const getStoredToken = () => {
    return Storage.get('token');
}

const getStoredProfile = () => {
    let user = null;

    const storedUser = Storage.get('user');

    if (storedUser) {
        user = new User(JSON.parse(storedUser));
    }

    return user;
}

const getCompanyCode = () => {
    return Storage.get('company_code');
}

/**
 * Always false, Unless we have a way to check token validity.
 *
 * For instance, if the expiration date is given when we get the token,
 * We could compare it here.
 */
const isTokenValid = () => {
    let isTokenValid = false;
    let expiration = Storage.get('token_expiration_date');

    if (expiration) {
        expiration = new moment(expiration);

        isTokenValid = expiration.isAfter();
    }

    return isTokenValid;
}

const initialState = {
    isAuthenticated: isTokenValid(),
    token: getStoredToken(),
    user: getStoredProfile(),
    companyCode: getCompanyCode(),
    error: ''
};

export default (state = initialState, action) => {
    switch (action.type) {
        case AuthenticationActionTypes.AUTHENTICATION_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: true,
                user: action.user,
                token: action.token,
                error: ''
            });
        case AuthenticationActionTypes.AUTHENTICATION_ERROR:
            return Object.assign({}, state, {
                isAuthenticated: false,
                user: null,
                token: null,
                error: action.error
            });
        case AuthenticationActionTypes.DISCONNECTION:
        case AuthenticationActionTypes.SWITCH_ACCOUNT_SUCCESS:
            return Object.assign({}, state, {
                isAuthenticated: false,
                user: null,
                token: null
            });
        default:
            return state;
    }
};
