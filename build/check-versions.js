var versionning   = require('semver')
var shell         = require('shelljs')
var chalk         = require('chalk')
var packageConfig = require('../package.json')

function executeSync(command) {
    return require('child_process')
        .execSync(command)
        .toString()
        .trim()
    ;
}

var versionRequirements = [
    {
        name: 'node',
        currentVersion: versionning.clean(process.version),
        versionRequirement: packageConfig.engines.node
    }
];

if (shell.which('npm')) {
    versionRequirements.push({
        name: 'npm',
        currentVersion: executeSync('npm --version'),
        versionRequirement: packageConfig.engines.npm
    });
}

module.exports = function () {
    var warnings = []

    for (var i = 0; i < versionRequirements.length; i++) {
        var mod = versionRequirements[i];

        if (!versionning.satisfies(mod.currentVersion, mod.versionRequirement)) {
            warnings.push(mod.name + ' : ' + chalk.red(mod.currentVersion) + ' should be updated to ' + chalk.green(mod.versionRequirement) + '.\n');
        }
    }

    if (warnings.length) {
        console.log('');
        console.log(chalk.yellow('To use this template, you must update following modules:'));
        console.log('');

        for (var i = 0; i < warnings.length; i++) {
            var warning = warnings[i];

            console.log('  ' + warning + '\n');
        }

        console.log('');
        process.exit(1);
    }
}
