import axios from 'axios';

/**
 * ApiClient
 *
 * This class is an HTTP client bootstrap for any external WebService or API
 * It needs to implement the different calls required by the application, always returning Promise objects.
 * Internally, it uses Axios as a base HTTP client.
 *
 * @see https://www.npmjs.com/package/axios
 *
 * NB : The application components use a defined format, therefore API returns should be formatted accordingly.
 * A dedicated class may be used to format such returns...
 */
class ApiClient {

    constructor(baseUrl) {
        this.instance = axios.create({
            baseUrl,
            // timeout: 30000,
            // headers: {}
        });
    }

    authenticate(username, password, companyCode) {
        return new Promise((resolve, reject) => {
            this.instance
                .post(`${this.baseUrl}/get_token`, {
                    username,
                    password
                })
                .then(response => {
                    // Eventually chain another API call to complete informations...
                    // Probably use another class to format the received data ?!
                    // @see actions/Authentication.es6 for the format
                    resolve({
                        token: response.body.token,
                        expires: null,
                        user: response.body.user
                    });
                }, error => {
                    reject(error);
                })
            ;
        });
    }

}

// baseURL is a parameter, injected at compilation by webpack.
// @see /config/prod.env.js
export default new ApiClient(process.env.API_BASE_URL);
