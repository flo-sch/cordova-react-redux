import { push } from 'react-router-redux';

// import DataProvider from '@/middlewares/Data/ApiClient';
import DataProvider from '@/middlewares/Data/StaticDataProvider';

/**
 * Action types
 */
export const VACATIONS_LOADED           = 'vacations.loaded';
export const VACATIONS_LOADING_FAILED   = 'vacations.loading_failed';

const onVacationsLoaded = (vacations) => {
    return {
        type: VACATIONS_LOADED,
        vacations
    }
}

const onVacationsLoadingFailed = (error) => {
    return {
        type: VACATIONS_LOADING_FAILED,
       error
    }
}

/**
 * Action creators
 */
export const loadVacations = () => {
    return dispatch => {
        DataProvider
            .loadVacations()
            .then(result => {
                const { vacations } = result;

                return dispatch(onVacationsLoaded(vacations));
            })
            .catch(error => {
                return dispatch(onVacationsLoadingFailed(error));
            })
        ;
    }
}

/**
 * Action types
 */
export const TEAM_PLANNING_LOADED           = 'vacations.team_planning.loaded';
export const TEAM_PLANNING_LOADING_FAILED   = 'vacations.team_planning.loading_failed';

const onTeamPlanningLoaded = (planning) => {
    return {
        type: TEAM_PLANNING_LOADED,
        planning
    }
}

const onTeamPlanningLoadingFailed = (error) => {
    return {
        type: TEAM_PLANNING_LOADING_FAILED,
       error
    }
}

/**
 * Action creators
 */
export const loadTeamPlanning = (week) => {
    return dispatch => {
        DataProvider
            .loadTeamPlanning(week)
            .then(result => {
                const { planning } = result;

                return dispatch(onTeamPlanningLoaded(planning));
            })
            .catch(error => {
                return dispatch(onTeamPlanningLoadingFailed(error));
            })
        ;
    }
}

/**
 * Action types
 */
export const VACATION_SIMULATED           = 'vacation.simulated';
export const VACATION_SIMULATION_FAILED   = 'vacation.simulation_failed';

const onVacationSimulated = (currentBalance, simulatedBalance, isSimulatedResultAllowed) => {
    return {
        type: VACATION_SIMULATED,
        currentBalance,
        simulatedBalance,
        isSimulatedResultAllowed
    };
}

const onVacationSimulationFailed = (error) => {
    return {
        type: VACATION_SIMULATION_FAILED,
        error
    };
}

/**
 * Action creators
 */
export const simulateVacation = (type, start, end) => {
    return dispatch => {
        // Call WS
        DataProvider
            .simulateVacation(type, start, end)
            .then(result => {
                const { currentBalance, simulatedBalance, isSimulatedResultAllowed } = result;

                return dispatch(onVacationSimulated(currentBalance, simulatedBalance, isSimulatedResultAllowed));
            })
            .catch(error => {
                return dispatch(onVacationSimulationFailed(error));
            })
        ;
    }
}

/**
 * Action types
 */
export const VACATION_CREATED           = 'vacation.created';
export const VACATION_CREATION_FAILED   = 'vacation.creation_failed';

const onVacationCreated = (vacation, balance) =>{
    return {
        type: VACATION_CREATED,
        vacation,
        balance
    };
}

const onVacationCreationFailed = (error) => {
    return {
        type: VACATION_CREATION_FAILED,
        error
    };
}

/**
 * Action creators
 */
export const createVacation = (type, start, end, comment) => {
    return dispatch => {
        // Call WS
        DataProvider
            .createVacation(type, start, end, comment)
            .then(result => {
                const { vacation, balance } = result;

                dispatch(onVacationCreated(vacation, balance));
                return dispatch(push('/time-off/new/success'));
            })
            .catch(error => {
                return dispatch(onVacationCreationFailed(error));
            })
        ;
    }
}
