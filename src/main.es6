// Import style to be included in HTML head as <style> tag
import '@/styles/app.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Redirect } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';

import History from '@/middlewares/History';
import Store from '@/store';
import { startRequest, endRequest } from '@/actions/Requests';
import App from '@/components/App';

import Authentication from '@/components/App/Authentication/AuthenticationComponent';
import Home from '@/components/App/Home/HomeComponent';
import Vacations from '@/components/App/Vacations/VacationsComponent';
import PendingVacations from '@/components/App/Vacations/PendingVacationsComponent';
import VacationsCalendar from '@/components/App/Vacations/VacationsCalendarComponent';
import VacationsBalances from '@/components/App/Vacations/VacationsBalancesComponent';
import NewVacation from '@/components/App/Vacations/NewVacationComponent';
import NewVacationCreated from '@/components/App/Vacations/NewVacationCreatedComponent';
import Expenses from '@/components/App/Expenses/ExpensesComponent';
import NewExpense from '@/components/App/Expenses/NewExpenseComponent';
import Meetings from '@/components/App/Meetings/MeetingsComponent';
import Planning from '@/components/App/Planning/PlanningComponent';

const PrivateRoute = ({ component: Component, ...props }) => (
    <Route { ...props } render={ props => {
            return Store.getState().AuthenticationReducer.isAuthenticated ? (
                <Component { ...props } />
            ) : (
                <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            );
        }
        } />
);

// Launch application
ReactDOM.render(
    <Provider store={ Store }>
        <ConnectedRouter history={ History }>
            <App>
                <Route exact path='/login' component={ Authentication } />
                <PrivateRoute exact path='/' component={ Home } />
                <PrivateRoute exact path='/time-off' component={ Vacations } />
                <PrivateRoute exact path='/time-off/pending' component={ PendingVacations } />
                <PrivateRoute exact path='/time-off/pending/calendar/:week' component={ VacationsCalendar } />
                <PrivateRoute exact path='/time-off/balances' component={ VacationsBalances } />
                <PrivateRoute exact path='/time-off/new' component={ NewVacation } />
                <PrivateRoute exact path='/time-off/new/success' component={ NewVacationCreated } />
                <PrivateRoute exact path='/expenses' component={ Expenses } />
                <PrivateRoute exact path='/expenses/new' component={ NewExpense } />
                <PrivateRoute exact path='/meetings' component={ Meetings } />
                <PrivateRoute exact path='/planning/:week' component={ Planning } />
            </App>
        </ConnectedRouter>
    </Provider>,
    document.getElementById('app-container'),
);

// Moment configuration
moment.locale('fr');

// Display loader on new HTTP request
axios.interceptors.request
    .use(config => {
        Store.dispatch(startRequest());
        return config;
    }, error => Promise.reject(error))
;

// Hide loader on HTTP response
axios.interceptors.response
    .use(response => {
        Store.dispatch(endRequest());
        return response;
    }, error => {
        Store.dispatch(endRequest());
        return Promise.reject(error);
    })
;
