/**
 * Action types
 */
export const HTTP_REQUEST_START = 'http.request.start';
export const HTTP_REQUEST_END = 'http.request.end';

const onRequestStart = () => {
    return {
        type: HTTP_REQUEST_START
    };
}

const onRequestEnd = () => {
    return {
        type: HTTP_REQUEST_END
    };
}

export const startRequest = () => {
    return dispatch => {
        return dispatch(onRequestStart());
    }
}

export const endRequest = () => {
    return dispatch => {
        return dispatch(onRequestEnd());
    }
}
