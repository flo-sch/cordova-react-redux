var path                = require('path')
var webpack             = require('webpack')
var ExtractTextPlugin   = require('extract-text-webpack-plugin')
var utils               = require('./utils')
var config              = require('../config')

function resolve(dir) {
    return path.join(__dirname, '..', dir);
}

module.exports = {
    entry: {
        app: './src/main.es6'
    },
    output: {
        path: config.build.assetsRoot,
        filename: '[name].js',
        publicPath: process.env.NODE_ENV === 'production'
            ? config.build.assetsPublicPath
            : config.dev.assetsPublicPath
    },
    resolve: {
        extensions: ['.js', '.jsx', '.es6', '.json', '.scss', '.css'],
        alias: {
            '@': resolve('src')
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx|es6)$/,
                loader: 'babel-loader',
                include: [
                    resolve('src')
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/i,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: utils.assetsPath('images/[name].[hash:7].[ext]')
                }
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader' // Create style node from JS string
                }, {
                    loader: 'css-loader' // Translate CSS into CommonJS
                }, {
                    loader: 'sass-loader', // Compile SASS to CSS
                    // options: {
                    //     includePaths: []
                    // }
                }]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: utils.assetsPath('fonts/[name].[hash:7].[ext]'),
                    publicPath: '../../'
                }
            }
        ]
    },
    plugins: [
        new webpack.BannerPlugin({
            banner: '// { "framework": "React" }\n',
            raw: true
        }),
        new ExtractTextPlugin({
            filename: "[name].[contenthash].css"
        })
    ]
}
