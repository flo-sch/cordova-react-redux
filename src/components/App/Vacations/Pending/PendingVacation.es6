import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Card, CardTitle, CardBlock, Collapse, Form, FormGroup, Input, Modal, ModalBody } from 'reactstrap';

// import { acceptVacation, forwardVacation, refuseVacation } from '@/actions/Vacations'; Not implemented yet!
import VacationModel from '@/models/Vacation';

class PendingVacation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isCollapsiblePanelOpen: false,
            comment: '',
            shouldRefusalModalBeDisplayed: false
        };

        this.collapse = this.collapse.bind(this);
        this.displayRefusalModal = this.displayRefusalModal.bind(this);
        this.closeRefusalModal = this.closeRefusalModal.bind(this);
        this.onCommentChange = this.onCommentChange.bind(this);
        this.handleAcceptationSubmit = this.handleAcceptationSubmit.bind(this);
        this.handleForwardSubmit = this.handleForwardSubmit.bind(this);
        this.handleRefusalSubmit = this.handleRefusalSubmit.bind(this);
    }

    collapse() {
        this.setState({
            isCollapsiblePanelOpen: !this.state.isCollapsiblePanelOpen
        });
    }

    displayRefusalModal() {
        this.setState({
            shouldRefusalModalBeDisplayed: true
        });
    }

    closeRefusalModal() {
        this.setState({
            shouldRefusalModalBeDisplayed: false
        });
    }

    onCommentChange(event) {
        this.setState({
            comment: event.target.value
        });
    }

    handleAcceptationSubmit(event) {
        event.preventDefault();

        this.props.acceptVacation();
        alert('Not implemented yet.');
    }

    handleForwardSubmit(event) {
        event.preventDefault();

        this.props.forwardVacation();
    }

    handleRefusalSubmit(event) {
        event.preventDefault();

        this.props.refuseVacation(this.state.comment);
    }

    render() {
        const { owner, start, end, comment } = this.props.vacation;
        /**
         * This will probably need to be adjusted depending on how the client count a day...
         * It keeps the night times for instance...
         *
         * Maybe easier to rely on a server-based information, that would be included in the Vacation object?
         */
        const duration = end.diff(start, 'days', true);

        return (
            <Card className='vacation'>
                <CardBlock>
                    <CardTitle onClick={ this.collapse }>
                        <div className='media'>
                            <span className={ 'd-flex prh-icon prh-icon-angle' + (this.state.isCollapsiblePanelOpen ? '' : ' rotate--90') }></span>
                            <div className='media-body'>
                                <strong>{ owner.lastname } { owner.firstname }</strong>
                                <div>
                                    <small>Du { start.format('L') } au { end.format('L') }, <strong>{ duration.toFixed(1) } j</strong></small>
                                </div>
                            </div>
                            <Link to={ `/time-off/pending/calendar/${(new moment()).format('w')}#${owner.id}` } className='d-flex align-self-center'>
                                <span className='round primary'>
                                    <span className='prh-icon prh-icon-calendar'></span>
                                </span>
                            </Link>
                        </div>
                    </CardTitle>
                    <Collapse isOpen={ this.state.isCollapsiblePanelOpen }>
                        <blockquote className='vacation-comment'>{ comment }</blockquote>
                        <button type='button' className='btn btn-block btn-info' onClick={ this.handleAcceptationSubmit }>Valider</button>
                        <button type='button' className='btn btn-block btn-outline-info btn-rounded' onClick={ this.handleForwardSubmit }>Transférer</button>
                        <button type='button' className='btn btn-block btn-outline-info btn-rounded' onClick={ this.displayRefusalModal }>Refuser</button>
                    </Collapse>
                </CardBlock>
                <Modal className='confirmation-modal' isOpen={ this.state.shouldRefusalModalBeDisplayed }>
                    <ModalBody>
                        <Form className='white' onSubmit={ this.handleRefusalSubmit }>
                            <header className='confirmation-modal-header'>
                                <span className='light light-danger'></span>
                                <p className='confirmation-status text-danger'>Motif du refus</p>
                            </header>
                            <div className='modal-block'>
                                <FormGroup>
                                    <Input type='textarea' name='comment' value={ this.state.comment } onChange={ this.onCommentChange } />
                                </FormGroup>
                            </div>
                            <div className='modal-block'>
                                <button type='submit' className='btn btn-block btn-info'>Refuser la demande</button>
                                <button type='button' className='btn btn-block btn-outline-info btn-rounded' onClick={ this.closeRefusalModal }>Fermer</button>
                            </div>
                        </Form>
                    </ModalBody>
                </Modal>
            </Card>
        );
    }
}

PendingVacation.propTypes = {
    vacation: PropTypes.instanceOf(VacationModel).isRequired
};

const mapDispatchToProps = (dispatch) => {
    return {
        acceptVacation: () => {
            // dispatch(acceptVacation());
            alert('Not implemented yet.');
        },
        forwardVacation: () => {
            // dispatch(forwardVacation());
            alert('Not implemented yet.');
        },
        refuseVacation: (comment) => {
            // dispatch(refuseVacation(comment));
            alert('Not implemented yet.');
        }
    }
}

export default connect(
    null,
    mapDispatchToProps
)(PendingVacation);
