// import DataProvider from '@/middlewares/Data/ApiClient';
import DataProvider from '@/middlewares/Data/StaticDataProvider';

/**
 * Action types
 */
export const BALANCES_LOADED           = 'balances.loaded';
export const BALANCES_LOADING_FAILED   = 'balances.loading_failed';

const onBalancesLoaded = (balances) => {
    return {
        type: BALANCES_LOADED,
        balances
    };
}

const onBalancesLoadingFailed = (error) => {
    return {
        type: BALANCES_LOADING_FAILED,
        error
    };
}

/**
 * Action creators
 */
export const loadBalances = () => {
    return dispatch => {
        // Call WS
        DataProvider
            .loadBalances()
            .then(result => {
                const { balances } = result;

                return dispatch(onBalancesLoaded(balances));
            })
            .catch(error => {
                return dispatch(onBalancesLoadingFailed(error));
            })
        ;
    }
}
