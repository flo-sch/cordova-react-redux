import * as RequestsActionTypes from '@/actions/Requests';

const initialState = {
    isLoading: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case RequestsActionTypes.HTTP_REQUEST_START:
            return Object.assign({}, state, {
                isLoading: true
            });
        case RequestsActionTypes.HTTP_REQUEST_END:
            return Object.assign({}, state, {
                isLoading: false
            });
        default:
            return state;
    }
}
