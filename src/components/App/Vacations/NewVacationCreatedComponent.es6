import React from 'react';
import { Link } from 'react-router-dom';

export default class NewVacationCreatedComponent extends React.Component {

    render() {
        return (
            <section id='success-feedback' className='full feedback text-center'>
                <div className='container-fluid'>
                    <div className='light-container light-container-lg'>
                        <figure className='light light-success'>
                            <img src={ require('@/static/images/check.svg') } alt='Check' width='90' />
                        </figure>
                    </div>
                    <h2 className='feedback-title'>Votre demande a bien été prise en compte</h2>
                    <p className='feedback-text'>
                        Vous pouvez suivre son évolution dans
                        <br />
                        <Link to='/time-off/pending' className='text-info'>Mes demandes</Link>
                    </p>
                    <br />
                    <br />
                    <Link to='/' className='btn btn-lg btn-block btn-info'>Revenir à l'accueil</Link>
                </div>
            </section>
        );
    }

}
