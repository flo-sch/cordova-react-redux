import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';

import { loadVacations } from '@/actions/Vacations';

class VacationsComponent extends React.Component {

    componentDidMount() {
        this.props.loadVacations();
    }

    render() {
        const { pending, mine } = this.props;

        return (
            <section id='vacations'>
                <header className='header header-lg gradient'>
                    <div className='container-fluid'>
                        <h2 className='header-title'>Absences</h2>
                        <div className='row background-logo' style={{ backgroundImage: `url(${require('@/static/images/logo.svg')})` }}>
                            <div className='col-6'>
                                <Link to='/time-off/new' className='wrapper-link'>
                                    <figure className='header-cta primary'>
                                        <span className='header-cta-icon prh-icon prh-icon-reminder'></span>
                                        <figcaption className='header-label'>Nouvelle<br />demande d'absence</figcaption>
                                        <button type='button' className='btn btn-sm btn-rounded btn-header btn-outline-primary'>
                                            <span className='prh-icon prh-icon-arrow'></span>
                                        </button>
                                    </figure>
                                </Link>
                            </div>
                            <div className='col-6'>
                                <Link to='/time-off/balances' className='wrapper-link'>
                                    <figure className='header-cta secondary'>
                                        <span className='header-cta-icon prh-icon prh-icon-user'></span>
                                        <figcaption className='header-label'>Mes<br />soldes</figcaption>
                                        <button type='button' className='btn btn-sm btn-rounded btn-header btn-outline-secondary'>
                                            <span className='prh-icon prh-icon-arrow'></span>
                                        </button>
                                    </figure>
                                </Link>
                            </div>
                        </div>
                        <Link to={{ pathname: `/planning/${(new moment()).format('w')}` }} className='btn btn-lg btn-info main-btn'>Planning</Link>
                    </div>
                </header>
                <div className='cards-list'>
                    <div className='card'>
                        <div className='card-block'>
                            <div className='media'>
                                <div className='media-body'>
                                    <h4 className='card-title'>{ mine.length }</h4>
                                    <p className='card-text'>demandes en cours</p>
                                </div>
                                <Link to='/time-off/pending#mine' className='d-flex align-self-center'>
                                    <span className='round primary'>
                                        <span className='prh-icon prh-icon-angle rotate-270'></span>
                                    </span>
                                </Link>
                            </div>
                        </div>
                    </div>
                    <div className='card'>
                        <div className='card-block'>
                            <div className='media'>
                                <div className='media-body'>
                                    <h4 className='card-title'>{ pending.length }</h4>
                                    <p className='card-text'>demandes à valider</p>
                                </div>
                                <Link to='/time-off/pending#pending' className='d-flex align-self-center'>
                                    <span className='round primary'>
                                        <span className='prh-icon prh-icon-angle rotate-270'></span>
                                    </span>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        pending: state.VacationsReducer.vacations.pending,
        mine: state.VacationsReducer.vacations.mine
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadVacations: () => {
            dispatch(loadVacations());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VacationsComponent);
