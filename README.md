# cordova-react-redux

> Cordova react + redux bootstrap app

## Build Setup

``` bash
# install dependencies
npm install

# Config

Create a `config/dev.env.js` file with your local configuration, following the sample :

```
var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"'
})
```

# Serve with hot reload at localhost:8080
npm run dev

# Build for production with minification
npm run build
```

## Cordova

Following scripts have bin created to simplify the cordova operations.

However, the `platforms` cordova-specific folder are not versionned,
To make it easier to compile undepending of the local SDK sources.

You just need to make sure to add at least one platform before starting :

##### Install cordova command line SDK globally
`npm i -g cordova`

##### Add a platform (possible values: android|ios|wp8) > https://cordova.apache.org/docs/en/latest/
`cordova platform add android`

```
# Build project as cordova app
npm run cordova-build

# Run cordova app into an android simulator
npm run cordova-emulate-android

# Run cordova app into an android device target
npm run cordova-run-android

# Build the android application with production configuration
npm run cordova-build-android
```
