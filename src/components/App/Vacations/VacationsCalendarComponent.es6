import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { push } from 'react-router-redux';
import moment from 'moment';
import Hammer from 'hammerjs';

import { loadTeamPlanning } from '@/actions/Vacations';
import BackButton from '@/components/Partials/BackButton';

class VacationsCalendarComponent extends React.Component {

    constructor(props) {
        super(props);

        this.handleSwipe = this.handleSwipe.bind(this);
    }

    componentDidMount() {
        const { week } = this.props.match.params;

        /**
         * Asynchronous data loading
         *
         * This could be better, once asynchronous resolvers will be available with react-router@4.
         *
         * @__UPDATE_LATER__
         */
        this.props.loadTeamPlanning(week);

        // Enable Swipe gesture thanks to HammerJS
        this.manager = new Hammer.Manager(ReactDOM.findDOMNode(this));
        const Swipe = new Hammer.Swipe({
            direction: Hammer.DIRECTION_HORIZONTAL,
            threshold: 10
        });
        this.manager.add(Swipe);

        // Listen to Swipe events
        this.manager.on('swipe', this.handleSwipe);
    }

    componentWillUnmount() {
        // Clear swipe event listener
        this.manager.off('swipe', this.handleSwipe);
    }

    /**
     * Asynchronous data loading
     *
     * This could be better, once asynchronous resolvers will be available with react-router@4.
     *
     * @__UPDATE_LATER__
     */
    componentWillReceiveProps(nextProps) {
        const routeChanged = nextProps.route !== this.props.route;

        if (routeChanged) {
            this.props.loadTeamPlanning(nextProps.match.params.week);
        }
    }

    handleSwipe(event) {
        const week = parseInt(this.props.match.params.week);

        switch (event.direction) {
            case Hammer.DIRECTION_LEFT:
                this.props.loadTeamWeek(week + 1);
                break;
            case Hammer.DIRECTION_RIGHT:
                this.props.loadTeamWeek(week - 1);
                break;
        }
    }

    get planning() {
        const { planning } = this.props;
        const week = parseInt(this.props.match.params.week);
        const weekStart = new moment().startOf('year').add(week, 'weeks').startOf('week');
        const weekEnd = new moment().startOf('year').add(week, 'weeks').endOf('week');

        let days = [];
        let currentDay = new moment(weekStart);

        while (currentDay.isBefore(weekEnd)) {
            days.push(new moment(currentDay));

            currentDay.add(1, 'days');
        }

        if (planning) {
            return (
                <div className='fixed-calendar-wrapper'>
                    <table className='team-calendar'>
                        <thead>
                            <tr>
                                <th></th>
                                { days.map((day, i) => {
                                    return (
                                        <th key={ i }>{ day.format('DD/MM') }</th>
                                    );
                                }) }
                            </tr>
                        </thead>
                        <tbody>
                            { planning.users.map((userRow, i) => {
                                return (
                                    <tr key={ `user-${i}` }>
                                        <td className='user-name'>{ `${userRow.user.firstname.charAt(0)}. ${userRow.user.lastname}` }</td>
                                        { userRow.days.map((day, j) => {
                                            let availability = day.available ? (
                                                    <div className='available'>&nbsp;</div>
                                                ) : (
                                                    <div className='unavailable'>A</div>
                                                )
                                            ;
                                            return (
                                                <td key={ `user-day-${j}` }>
                                                    { availability }
                                                </td>
                                            );
                                        }) }
                                    </tr>
                                );
                            }) }
                        </tbody>
                    </table>
                </div>
            );
        }
    }

    render() {
        const week = parseInt(this.props.match.params.week);
        const weekStart = new moment().startOf('year').add(week, 'weeks').startOf('week');
        const weekEnd = new moment().startOf('year').add(week, 'weeks').endOf('week');

        return (
            <section>
                <header className='header gradient'>
                    <BackButton navigate={ this.props.navigate } />
                    <h2 className='header-title'>Demandes à valider</h2>
                </header>
                <div className='calendar-controls'>
                    <Link to={{ pathname: `/time-off/pending/calendar/${(week - 1)}` }} className='calendar-control-left'>
                        <span className='prh-icon prh-icon-angle rotate-90'></span>
                    </Link>
                    <h3 className='calendar-caption'>Semaine { week }</h3>
                    <p className='calendar-week-description'>du { weekStart.format('L') } au { weekEnd.format('L') }</p>
                    <Link to={{ pathname: `/time-off/pending/calendar/${(week + 1)}` }} className='calendar-control-right'>
                        <span className='prh-icon prh-icon-angle rotate-270'></span>
                    </Link>
                </div>
                { this.planning }
            </section>
        );
    }

}


const mapStateToProps = (state) => {
    return {
        hash: state.routing.location.hash,
        route: state.routing.location.pathname,
        planning: state.VacationsReducer.planning
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        navigate: () => {
            dispatch(push('/time-off/pending'));
        },
        loadTeamWeek: (week) => {
            dispatch(push(`/time-off/pending/calendar/${week}`));
        },
        loadTeamPlanning: (week) => {
            dispatch(loadTeamPlanning(week));
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(VacationsCalendarComponent);
