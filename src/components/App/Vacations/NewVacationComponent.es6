import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Form, FormGroup, Input, InputGroup, InputGroupAddon, Modal, ModalBody } from 'reactstrap';

import { simulateVacation, createVacation } from '@/actions/Vacations';
import BackButton from '@/components/Partials/BackButton';

class NewVacationComponent extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            type: 'CR',
            start: '',
            startMode: 'day', // 'morning' || 'afternoon'
            end: '',
            endMode: 'day', // 'morning' || 'afternoon'
            comment: '',
            shouldModalBeDisplayed: false
        };

        this.onTypeChange = this.onTypeChange.bind(this);
        this.onStartChange = this.onStartChange.bind(this);
        this.onStartModeChange = this.onStartModeChange.bind(this);
        this.onEndChange = this.onEndChange.bind(this);
        this.onEndModeChange = this.onEndModeChange.bind(this);
        this.onCommentChange = this.onCommentChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.displayModal = this.displayModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onConfirmation = this.onConfirmation.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.simulatedBalance) {
            this.displayModal();
        }
    }

    onTypeChange(event) {
        this.setState({
            type: event.target.value
        });
    }

    onStartChange(event) {
        this.setState({
            start: event.target.value
        });
    }

    onStartModeChange(event) {
        this.setState({
            startMode: event.target.value
        });
    }

    onEndChange(event) {
        this.setState({
            end: event.target.value
        });
    }

    onEndModeChange(event) {
        this.setState({
            endMode: event.target.value
        });
    }

    onCommentChange(event) {
        this.setState({
            comment: event.target.value
        });
    }

    displayModal() {
        this.setState({
            shouldModalBeDisplayed: true
        });
    }

    closeModal() {
        this.setState({
            shouldModalBeDisplayed: false
        });
    }

    getStartDayPeriod() {
        let startDayPeriod = 'matin';

        switch (this.state.startMode) {
            case 'afternoon':
                startDayPeriod = 'après-midi';
        }

        return startDayPeriod;
    }

    getEndDayPeriod() {
        let endDayPeriod = 'soir';

        if (this.state.startMode !== 'day') {
            switch (this.state.endMode) {
                case 'morning':
                    endDayPeriod = 'matin';
            }
        }

        return endDayPeriod;
    }

    get start() {
        let start = new moment(this.state.start);

        switch (this.state.startMode) {
            case 'day':
            case 'morning':
                start
                    .hours(9)
                    .minute(0)
                    .seconds(0)
                ;
                break;
            case 'afternoon':
                start
                    .hours(13)
                    .minute(0)
                    .seconds(0)
                ;
                break;
        }

        return start;
    }

    get end() {
        let end = new moment(this.state.end);

        if (this.state.startMode === 'day') {
            end = new moment(this.state.start);
            end
                .hours(18)
                .minute(0)
                .seconds(0)
            ;
        } else {
            switch (this.state.endMode) {
                case 'morning':
                    end
                        .hours(12)
                        .minute(0)
                        .seconds(0)
                    ;
                    break;
                case 'day':
                    end
                        .hours(18)
                        .minute(0)
                        .seconds(0)
                    ;
                    break;
            }
        }

        return end;
    }

    handleSubmit(event) {
        event.preventDefault();

        // Simulate balance for this type of vacation
        // Notice the use of :
        // this.start ---> internally calls get start()
        // this.end ---> internally calls get end()
        // Instead of this.state.start and this.state.end which does not include the startMode / endMode
        this.props.simulateVacation(this.state.type, this.start, this.end);
    }

    onVacationSimulated() {
        this.setState({
            shouldModalBeDisplayed: true
        });
    }

    onConfirmation() {
        // Notice the use of :
        // this.start ---> internally calls get start()
        // this.end ---> internally calls get end()
        // Instead of this.state.start and this.state.end which does not include the startMode / endMode
        this.props.createVacation(this.state.type, this.start, this.end);
    }

    get modal() {
        let statusName = 'success';
        let introduction = 'Le cumul de ce type d\'absences prendrait alors une valeur négative, toutefois autorisée.';
        let confirmationButton = (
            <button onClick={ this.onConfirmation } className='btn btn-block btn-info'>Confirmer</button>
        );
        let cancelButton = (
            <button onClick={ this.closeModal } className='btn btn-block btn-rounded btn-outline-info'>Annuler</button>
        );

        if (this.props.simulatedBalance < 0) {
            if (this.props.isSimulatedResultAllowed) {
                introduction = 'Le cumul de ce type d\'absences prendrait alors une valeur négative, toutefois autorisée.';
            } else {
                statusName = 'danger';
                introduction = 'Le cumul de ce type d\'absences prendrait alors une valeur négative, ce qui n\'est pas autorisé.';
                confirmationButton = null;
                cancelButton = (
                    <button onClick={ this.closeModal } className='btn btn-block btn-info'>Annuler</button>
                );
            }
        }

        return (
            <Modal className='confirmation-modal' isOpen={ this.state.shouldModalBeDisplayed }>
                <ModalBody>
                    <header className='confirmation-modal-header'>
                        <span className={ `light light-${statusName}` }></span>
                        <p className={ `confirmation-status text-${statusName}` }>{ introduction }</p>
                    </header>
                    <div className='modal-block'>
                        Demande : <strong>{ this.typeName }</strong>
                        <br />
                        Du { new moment(this.start).format('L') } ({ this.getStartDayPeriod() })
                        <br />
                        Au { new moment(this.end).format('L') } ({ this.getEndDayPeriod() })
                    </div>
                    <div className='modal-block'>
                        <strong>INFORMATION</strong>
                        <br />
                        Solde avant : { `${this.props.currentBalance}j` }
                        <br />
                        Solde après : { `${this.props.simulatedBalance}j` }
                    </div>
                    <div className='modal-block'>
                        { confirmationButton }
                        { cancelButton }
                    </div>
                </ModalBody>
            </Modal>
        );
    }

    get typeName() {
        let typeName = 'Congés payées';

        switch (this.state.type) {
            case 'RTT':
                typeName = 'Réduction de Temps de Travail';
                break;
            case 'EXPERIENCE':
                typeName = 'Ancienneté';
                break;
            case 'BREAK':
                typeName = 'Arrêt';
                break;
            case 'MATERNITY_LEAVE':
                typeName = 'Congés maternité';
                break;
            case 'PATERNITY_LEAVE':
                typeName = 'Congés paternité';
                break;
            case 'NO_WORK':
                typeName = 'Journée non travaillée';
                break;
            case 'FREE':
                typeName = 'Congés sans solde';
                break;
        }

        return typeName;
    }

    render() {
        return (
            <section id='new-time-off'>
                <header className='header gradient'>
                    <BackButton />
                    <h2 className='header-title'>Demande d'absence</h2>
                </header>
                <Form className='white' onSubmit={ this.handleSubmit }>
                    <FormGroup>
                        <Input type='select' name='type' required value={ this.state.type } onChange={ this.onTypeChange }>
                            <option value='CP'>Congés payées</option>
                            <option value='RTT'>RTT</option>
                            <option value='EXPERIENCE'>Ancienneté</option>
                            <option value='BREAK'>Pont</option>
                            <option value='MATERNITY_LEAVE'>Congés Maternité</option>
                            <option value='PATERNITY_LEAVE'>Congés Paternité</option>
                            <option value='NO_WORK'>Journée non travaillée</option>
                            <option value='FREE'>Congés sans solde</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <label htmlFor='start'>Du (inclus)</label>
                        <Input type='date' id='start' name='start' required placeholder='Du (inclus)' className='form-control' value={ this.state.start } onChange={ this.onStartChange } />
                        <br />
                        <Input type='select' name='startMode' value={ this.state.startMode } onChange={ this.onStartModeChange }>
                            <option value='day'>Journée entière</option>
                            <option value='morning'>Matin</option>
                            <option value='afternoon'>Après-midi</option>
                        </Input>
                    </FormGroup>
                    <FormGroup className={ this.state.startMode == 'day' ? 'hidden' : '' }>
                        <label htmlFor='end'>Au (inclus)</label>
                        <Input type='date' id='end' name='end' required={ this.state.startMode != 'day' } min={ this.start.format('YYYY-MM-DD') } placeholder='Au (inclus)' className='form-control' value={ this.state.end } onChange={ this.onEndChange } />
                        <br />
                        <Input type='select' name='endMode' required={ this.state.startMode != 'day' } value={ this.state.endMode } onChange={ this.onEndModeChange }>
                            <option value='day'>Journée entière</option>
                            <option value='morning'>Matin</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Input type='textarea' placeholder='Commentaire optionnel...' name='comment' value={ this.state.comment } onChange={ this.onCommentChange } />
                    </FormGroup>
                    <FormGroup>
                        <button type='submit' className='btn btn-block btn-info'>Valider</button>
                        <Link to='/time-off' className='btn btn-block btn-rounded btn-outline-info'>Annuler</Link>
                    </FormGroup>
                </Form>
                { this.modal }
            </section>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        currentBalance: state.VacationsReducer.currentBalance,
        simulatedBalance: state.VacationsReducer.simulatedBalance,
        isSimulatedResultAllowed: state.VacationsReducer.isSimulatedResultAllowed
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        simulateVacation: (type, start, end) => {
            dispatch(simulateVacation(type, start, end));
        },
        createVacation: (type, start, end, comment) => {
            dispatch(createVacation(type, start, end, comment, comment));
        }
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewVacationComponent);
