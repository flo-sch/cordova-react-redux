import React from 'react';

export default class Loader extends React.Component {

    render() {
        return (
            <aside className='loader-overlay'>
                <div className='loader-container'>
                    <div className='spinner'></div>
                </div>
            </aside>
        );
    }

}
