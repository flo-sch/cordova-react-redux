import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import AuthenticationReducer from '@/reducers/AuthenticationReducer';
import HttpReducer from '@/reducers/HttpReducer';
import PlanningReducer from '@/reducers/PlanningReducer';
import VacationsReducer from '@/reducers/VacationsReducer';
import BalancesReducer from '@/reducers/BalancesReducer';

export default combineReducers({
    AuthenticationReducer,
    HttpReducer,
    PlanningReducer,
    VacationsReducer,
    BalancesReducer,
    routing: routerReducer,
});
