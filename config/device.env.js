var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"device"',
    API_BASE_URL: '"https://api.test.example.com/"'
})
