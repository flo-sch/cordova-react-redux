import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import PlanningRow from '@/components/App/Planning/Calendar/PlanningRow';

export default class Planning extends React.Component {

    render() {
        return (
            <table className='planning-calendar'>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Lieu</th>
                        <th>Plage</th>
                    </tr>
                </thead>
                <tbody>
                    { this.props.days.map((day, i) => {
                        return <PlanningRow
                            key={ i }
                            date={ day.date }
                            events={ day.events }
                            />
                    }) }
                </tbody>
            </table>
        );
    }
}

Planning.propTypes = {
    days: PropTypes.arrayOf(PropTypes.shape({
        date: PropTypes.instanceOf(moment).isRequired,
        events: PropTypes.arrayOf(PropTypes.shape({
            type: PropTypes.string.isRequired,
            place: PropTypes.string.isRequired,
            time: PropTypes.string.isRequired
        })).isRequired
    }))
};
