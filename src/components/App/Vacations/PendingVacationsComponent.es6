import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';

import { loadVacations } from '@/actions/Vacations';
import PendingVacation from '@/components/App/Vacations/Pending/PendingVacation';
import BackButton from '@/components/Partials/BackButton';

class PendingVacationsComponent extends React.Component {

    constructor(props) {
        super(props);

        const { hash } = props;
        let activeTab = 'pending';

        switch (hash) {
            case '#pending':
            case '#mine':
                activeTab = hash.substr(1);
            break;
        }

        this.state = {
            activeTab: activeTab, // 'pending' || 'mine'
        }

        this.switchTab = this.switchTab.bind(this);
    }

    componentDidMount() {
        this.props.loadVacations();
    }

    switchTab(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }

    render() {
        const { pending, mine } = this.props;

        return (
            <section>
                <header className='header header-lg gradient'>
                    <BackButton />
                    <h2 className='header-title'>Absences</h2>
                    <Nav tabs className='nav-fill'>
                        <NavItem>
                            <NavLink className={ this.state.activeTab === 'pending' ? 'active' : '' } onClick={ () => { this.switchTab('pending'); } }>
                                { pending.length } à valider
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink className={ this.state.activeTab === 'mine' ? 'active' : '' } onClick={ () => { this.switchTab('mine'); } }>
                                Mes demandes
                            </NavLink>
                        </NavItem>
                    </Nav>
                </header>
                <TabContent activeTab={ this.state.activeTab }>
                    <TabPane tabId='pending'>
                        { pending.map((vacation, i) => {
                            return <PendingVacation
                                key={ i }
                                vacation={ vacation }
                                />
                        }) }
                    </TabPane>
                    <TabPane tabId='mine'>
                        { mine.map((vacation, i) => {
                            return <PendingVacation
                                key={ i }
                                vacation={ vacation }
                                />
                        }) }
                    </TabPane>
                </TabContent>
            </section>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        hash: state.routing.location.hash,
        pending: state.VacationsReducer.vacations.pending,
        mine: state.VacationsReducer.vacations.mine
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadVacations: () => {
            dispatch(loadVacations());
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PendingVacationsComponent);
