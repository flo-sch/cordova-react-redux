import { Record } from 'immutable';

const Vacation = Record({
    id: null,
    type: null,
    start: null,
    end: null,
    validated_at: null,
    owner: null,
    comment: ''
}, 'Vacation');

export default Vacation;
