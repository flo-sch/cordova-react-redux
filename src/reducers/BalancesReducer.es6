import * as BalancesActionTypes from '@/actions/Balances';

const initialState = {
    balances: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case BalancesActionTypes.BALANCES_LOADED:
            return Object.assign({}, state, {
                balances: action.balances
            });
        case BalancesActionTypes.BALANCES_LOADING_FAILED:
            return Object.assign({}, state, {
                balances: []
            });
        default:
            return state;
    }
}
