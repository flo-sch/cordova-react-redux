import uuid from 'uuid';
import moment from 'moment';

import Store from '@/store';
import { startRequest, endRequest } from '@/actions/Requests';
import User from '@/models/User';
import Vacation from '@/models/Vacation';

const getRandomNumberBetween = (min, max, decimals = 2) => {
    return (Math.random() * (max - min) + min).toFixed(decimals);
}

/**
 * This class is a static fake data provider to test the application workflow
 * It has to be replaced once web services will be available
 *
 * We just set a timeout to fake the asynchronous loading time...
 */
class StaticDataProvider {

    /**
     * Return an array of fake users
     *
     * @return array
     */
    getUsers() {
        return {
            me: new User({ id: 1, username: 'XX-00000', firstname: 'Pierre', lastname: 'MICHET', email: 'pierre@example.org', avatar: null }),
            jeanJacques: new User({ id: 2, username: 'YX-00123', firstname: 'Jean-Jacques', lastname: 'MARTIN', email: 'jj.martin@example.org', avatar: null }),
            marie: new User({ id: 3, username: 'XX-09483', firstname: 'Marie', lastname: 'DUPONT', email: 'marie.dupont@example.org', avatar: null }),
            jeanLuc: new User({ id: 4, username: 'YY-38473', firstname: 'Jean-Luc', lastname: 'CHAUX', email: 'jp.chaux@example.org', avatar: null }),
            john: new User({ id: 5, username: 'YX-19874', firstname: 'John', lastname: 'DOE', email: 'jdoe@example.org', avatar: null }),
            mickael: new User({ id: 6, username: 'ZZ-00042', firstname: 'Mickael', lastname: 'BAR', email: 'mbar@example.org', avatar: null })
        }
    }

    /**
     * Simulate a fake HTTP request
     *
     * It will run a timeout method, resolving a Promise with formatted data after a random duration.
     *
     * @param object data : a JavaScript object of formatted data.
     * @return Promise
     */
    simulateRequest(data) {
        Store.dispatch(startRequest());

        return new Promise((resolve, reject) => {
            this.timeout = setTimeout(() => {
                Store.dispatch(endRequest());
                resolve(data);
            }, getRandomNumberBetween(200, 1600, 0));
        });
    }

    authenticate(username, password, companyCode) {
        const { me } = this.getUsers();

        return this.simulateRequest({
            token: uuid.v4(),
            expires: new moment().add(20, 'minutes'),
            user: me
        });
    }

    loadPlanning(week) {
        let start = new moment();

        start
            .startOf('year')
            .add(week, 'weeks')
            .startOf('week')
        ;

        const monday    = new moment(start);
        const tuesday   = new moment(start).add(1, 'day');
        const wednesday = new moment(start).add(2, 'day');
        const thursday  = new moment(start).add(3, 'day');
        const friday    = new moment(start).add(4, 'day');
        const saturday  = new moment(start).add(5, 'day');

        return this.simulateRequest({
            days: [
                {
                    date: monday, // Date should be a moment object (so the view component would be able to format it).
                    events: [
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: '09:00 - 12:00' // Can result of a date calculation if needed, thanks to moment.js...
                        },
                        {
                            type: 'time-off',
                            place: 'CP',
                            time: '13:00 17:00',
                        }
                    ]
                },
                {
                    date: tuesday,
                    events: [
                        {
                            type: 'time-off',
                            place: 'CP',
                            time: 'Journée'
                        }
                    ]
                },
                {
                    date: wednesday,
                    events: [
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: '09:00 - 12:00'
                        },
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: '13:00 - 17:00'
                        }
                    ]
                },
                {
                    date: thursday,
                    events: [
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: '09:00 - 12:00'
                        },
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: '14:00 - 18:00'
                        }
                    ]
                },
                {
                    date: friday,
                    events: [
                        {
                            type: 'direction',
                            place: 'Direction Technique',
                            time: 'Journée'
                        }
                    ]
                },
                {
                    date: saturday,
                    events: []
                }
            ]
        });
    }

    simulateVacation(type, start, end) {
        return this.simulateRequest({
            // Well... It's fake data anyway.
            currentBalance: getRandomNumberBetween(-3, 12),
            simulatedBalance: getRandomNumberBetween(-5, 5),
            isSimulatedResultAllowed: Math.random() > 0.5
        });
    }

    createVacation(type, start, end, comment = '') {
        const { me } = this.getUsers();

        return this.simulateRequest({
            vacation: new Vacation({
                id: 1,
                type: type,
                start: start, // We just take the created start and end...
                end: end, // We just take the created start and end...,
                validated_at: null, // Vacation has not been validated yet
                owner: me,
                comment: 'Nodal point order-flow systemic Kowloon ablative voodoo god military-grade dome cardboard paranoid physical narrative engine.'
            }),
            balance: getRandomNumberBetween(-5, 5), // Well... It's fake data anyway.
        });
    }

    loadBalances() {
        return this.simulateRequest({
            balances: [
                {
                    name: 'Congés payés',
                    history: [
                        { name: '001. CCP', balance: -1, futureBalance: 3 },
                        { name: '002. CP acquis', balance: 18.75, futureBalance: 3 },
                        { name: '003. CP pris', balance: -20, futureBalance: 3 },
                        { name: '004. CP ancienneté', balance: 0, futureBalance: 3 },
                        { name: '005. CP ancienneté pris', balance: 0, futureBalance: 3 }
                    ]
                },
                {
                    name: 'RTT',
                    history: [
                        { name: '010. RTT acquis', balance: 6.5, futureBalance: 6.5 },
                        { name: '011. RTT pris', balance: -4.5, futureBalance: 2 },
                        { name: '012. RTT ancienneté', balance: 2, futureBalance: 4 },
                        { name: '013. RTT ancienneté pris', balance: -2, futureBalance: 2 }
                    ]
                },
                {
                    name: 'Compteurs',
                    history: []
                }
            ]
        });
    }

    loadVacations() {
        const { me, jeanJacques, marie, jeanLuc, john, mickael } = this.getUsers();

        return this.simulateRequest({
            vacations: {
                pending: [
                    new Vacation({ id: 1, type: 'CP', start: new moment('2017-07-01 09:00:00'), end: new moment('2017-07-03 18:00:00'), validated_at: null, owner: jeanJacques, comment: 'Motivation is the driving force behind life-enhancing change. It comes from knowing exactly what you want to do and having an insatiable, burning desire to do what’s.' }),
                    new Vacation({ id: 2, type: 'CP', start: new moment('2017-07-12 09:00:00'), end: new moment('2017-07-14 12:00:00'), validated_at: null, owner: marie, comment: 'Girl free-market car youtube euro-pop ablative fluidity disposable saturation point advert singularity drugs spook convenience store numinous footage.' }),
                    new Vacation({ id: 3, type: 'RTT', start: new moment('2017-07-15 13:00:00'), end: new moment('2017-07-15 18:00:00'), validated_at: null, owner: jeanLuc, comment: 'Shanty town motion-space BASE jump Tokyo plastic wonton soup. J-pop lights Chiba alcohol warehouse fetishism grenade free-market.' }),
                    new Vacation({ id: 4, type: 'CP', start: new moment('2017-07-22 13:00:00'), end: new moment('2017-07-22 18:00:00'), validated_at: null, owner: john, comment: 'Convenience store math-range-rover dissident courier cyber-stimulate urban wonton soup katana city smart-jeans nano.-space order-flow RAF artisanal smart-meta-drugs Shibuya narrative.' }),
                    new Vacation({ id: 5, type: 'RTT', start: new moment('2017-07-23 09:00:00'), end: new moment('2017-07-23 18:00:00'), validated_at: null, owner: mickael, comment: '' }),
                    new Vacation({ id: 6, type: 'CP', start: new moment('2017-07-28 13:00:00'), end: new moment('2017-07-30 18:00:00'), validated_at: null, owner: jeanLuc, comment: 'Soul-delay pistol network apophenia human futurity denim crypto. Chrome network voodoo god smart-rain denim Tokyo. ' }),
                    new Vacation({ id: 7, type: 'RTT', start: new moment('2017-07-30 09:00:00'), end: new moment('2017-07-31 12:00:00'), validated_at: null, owner: jeanJacques, comment: 'Advert render-farm lights shanty town cyber-pistol decay human claymore mine franchise boat marketing monofilament.' })
                ],
                mine: [
                    new Vacation({ id: 1, type: 'CP', start: new moment('2017-07-20 09:00:00'), end: new moment('2017-07-20 18:00:00'), validated_at: null, owner: me, comment: 'Futurity Legba chrome media nodality tanto girl youtube Tokyo RAF A.I. uplink face forwards pre-dome.' }),
                    new Vacation({ id: 2, type: 'RTT', start: new moment('2017-07-26 13:00:00'), end: new moment('2017-07-27 12:00:00'), validated_at: null, owner: me, comment: 'Sensory gang human wristwatch tank-traps pre-singularity futurity realism convenience store engine semiotics math-cyber-sign car marketing.' }),
                ]
            }
        })
    }

    loadTeamPlanning(week) {
        const { me, jeanJacques, marie, jeanLuc, john, mickael } = this.getUsers();

        let start = new moment();
        start
            .startOf('year')
            .add(week, 'weeks')
            .startOf('week')
        ;
        let end   = new moment(start);
        end
            .add(1, 'week')
            .subtract(1, 'seconds')
        ;

        const monday    = new moment(start);
        const tuesday   = new moment(start).add(1, 'day');
        const wednesday = new moment(start).add(2, 'day');
        const thursday  = new moment(start).add(3, 'day');
        const friday    = new moment(start).add(4, 'day');
        const saturday  = new moment(start).add(5, 'day');
        const sunday    = new moment(start).add(6, 'day');

        return this.simulateRequest({
            planning: {
                start,
                end,
                days: end.diff(start, 'days'),
                users: [
                    {
                        user: jeanJacques,
                        days: [
                            { date: monday, available: (Math.random() > 0.5) },
                            { date: tuesday, available: (Math.random() > 0.5) },
                            { date: wednesday, available: (Math.random() > 0.5) },
                            { date: thursday, available: (Math.random() > 0.5) },
                            { date: friday, available: (Math.random() > 0.5) },
                            { date: saturday, available: true },
                            { date: sunday, available: true }
                        ]
                    },
                    {
                        user: marie,
                        days: [
                            { date: monday, available: (Math.random() > 0.5) },
                            { date: tuesday, available: (Math.random() > 0.5) },
                            { date: wednesday, available: (Math.random() > 0.5) },
                            { date: thursday, available: (Math.random() > 0.5) },
                            { date: friday, available: (Math.random() > 0.5) },
                            { date: saturday, available: true },
                            { date: sunday, available: true }
                        ]
                    },
                    {
                        user: jeanLuc,
                        days: [
                            { date: monday, available: (Math.random() > 0.5) },
                            { date: tuesday, available: (Math.random() > 0.5) },
                            { date: wednesday, available: (Math.random() > 0.5) },
                            { date: thursday, available: (Math.random() > 0.5) },
                            { date: friday, available: (Math.random() > 0.5) },
                            { date: saturday, available: true },
                            { date: sunday, available: true }
                        ]
                    },
                    {
                        user: john,
                        days: [
                            { date: monday, available: (Math.random() > 0.5) },
                            { date: tuesday, available: (Math.random() > 0.5) },
                            { date: wednesday, available: (Math.random() > 0.5) },
                            { date: thursday, available: (Math.random() > 0.5) },
                            { date: friday, available: (Math.random() > 0.5) },
                            { date: saturday, available: true },
                            { date: sunday, available: true }
                        ]
                    },
                    {
                        user: mickael,
                        days: [
                            { date: monday, available: (Math.random() > 0.5) },
                            { date: tuesday, available: (Math.random() > 0.5) },
                            { date: wednesday, available: (Math.random() > 0.5) },
                            { date: thursday, available: (Math.random() > 0.5) },
                            { date: friday, available: (Math.random() > 0.5) },
                            { date: saturday, available: true },
                            { date: sunday, available: true }
                        ]
                    }
                ]
            }
        });
    }

}

export default new StaticDataProvider();
