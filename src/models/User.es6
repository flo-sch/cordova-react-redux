import { Record } from 'immutable';

const User = Record({
    id: null,
    username: null,
    firstname: null,
    lastname: null,
    email: null,
    avatar: null
}, 'User');

export default User;
