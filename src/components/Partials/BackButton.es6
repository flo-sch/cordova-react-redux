import React from 'react';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

class BackButton extends React.Component {

    render() {
        return (
            <a onClick={ this.props.navigate } className='btn btn-back btn-sm btn-rounded btn-link'>
                <span className='prh-icon prh-icon-arrow rotate-180'></span>
            </a>
        );
    }

}

const mapDispatchToProps = (dispatch, ownProps) => {
    let navigate = () => {
        dispatch(goBack());
    }

    if (typeof ownProps.navigate === 'function') {
        navigate = () => {
            dispatch(ownProps.navigate);
        }
    }

    return {
        navigate: navigate
    }
}

export default connect(
    null,
    mapDispatchToProps
)(BackButton);
